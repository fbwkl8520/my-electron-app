//app:控制应用生命周期的模块。
//BrowserView:创建原生浏览器窗口的模块
const {app,BrowserWindow} = require('electron');
const path=require('path');
const url=require('url');

// 保持一个对于 window 对象的全局引用，不然，当 JavaScript 被 GC，
// window 会被自动地关闭
var mainWindow = null;

// 当所有窗口被关闭了，退出。
app.on('window-all-closed', function() {
  // 在 OS X 上，通常用户在明确地按下 Cmd + Q 之前
  // 应用会保持活动状态
  if (process.platform != 'darwin') {
    app.quit();
  }
});
// 当 Electron 完成了初始化并且准备创建浏览器窗口的时候
// 这个方法就被调用
app.on('ready', function() {
  createWindow();
});
function createWindow(){
  //创建窗口
  mainWindow=new BrowserWindow({
    width:800,
    height:500,
    webPreferences:{
      nodeIntergration:true
    }
  });
  //加载应用html
  const indexPath=url.pathToFileURL(path.join(__dirname,'index.html')).href;
  //mainWindow.loadFile(encodeURI(indexPath));
  mainWindow.loadURL(indexPath);
  //打开调试工具
  mainWindow.openDevTools();
  mainWindow.on('close',function(){
    mainWindow=null;
    app.quit();
  });
}